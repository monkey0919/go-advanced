package main

import (
	"fmt"
	"time"
)

func main() {
	// 1, 获取当前时间
	t1 := time.Now()
	fmt.Printf("%T\n", t1)
	fmt.Println(t1)

	// 2，获取指定时间
	t2 := time.Date(2021, 6, 6, 0, 0, 0, 0, time.Local)
	fmt.Println(t2)

	// 3，格式化字符串对象
	f1 := t1.Format("2006年1月2日 15:04:05")
	fmt.Println(f1)

	// 4, 字符串转时间对象
	t3, _ := time.Parse("2006年1月2日 15:04:05", f1)
	fmt.Println(t3)

	// 5，转换字符串
	f2 := t1.String()
	fmt.Println(f2)

	// 6，时间对象获取年月日
	year, month, day := t2.Date()
	fmt.Println(year, int(month), day)

	// 7，获取时分秒
	hour, min, sec := t1.Clock()
	fmt.Println(hour, min, sec)

	// 8，获取时间戳
	timestamp := time.Now().Unix()
	fmt.Println(timestamp)

	timestamp1 := time.Now().UnixNano()
	fmt.Println(timestamp1)

	// 9，时间间隔
	t4 := t1.Add(time.Hour * 24)
	fmt.Println(t4)

	// 10，时间间隔
	t5 := t4.Sub(t1)
	fmt.Println(t5)
}
